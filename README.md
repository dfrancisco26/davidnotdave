# Personal Page

## Resources used

- Next.js
- Anime.js
- AOS
- Miro
- Google Fonts
- Coolors.co

### Style Stuff

<!-- <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@200;300;400&family=Oxygen:wght@300;400&display=swap" rel="stylesheet"> 

/* CSS HEX */
--gainsboro: #cdddddff;
--razzmatazz: #db3069ff;
--orange-web: #ffa400ff;
--shadow: #7b7263ff;
--rich-black-fogra-29: #04080fff;

/* CSS HSL */
--gainsboro: hsla(180, 19%, 84%, 1);
--razzmatazz: hsla(340, 70%, 52%, 1);
--orange-web: hsla(39, 100%, 50%, 1);
--shadow: hsla(38, 11%, 44%, 1);
--rich-black-fogra-29: hsla(218, 58%, 4%, 1);

/* SCSS HEX */
$gainsboro: #cdddddff;
$razzmatazz: #db3069ff;
$orange-web: #ffa400ff;
$shadow: #7b7263ff;
$rich-black-fogra-29: #04080fff;

/* SCSS HSL */
$gainsboro: hsla(180, 19%, 84%, 1);
$razzmatazz: hsla(340, 70%, 52%, 1);
$orange-web: hsla(39, 100%, 50%, 1);
$shadow: hsla(38, 11%, 44%, 1);
$rich-black-fogra-29: hsla(218, 58%, 4%, 1);

/* SCSS RGB */
$gainsboro: rgba(205, 221, 221, 1);
$razzmatazz: rgba(219, 48, 105, 1);
$orange-web: rgba(255, 164, 0, 1);
$shadow: rgba(123, 114, 99, 1);
$rich-black-fogra-29: rgba(4, 8, 15, 1);

/* SCSS Gradient */
$gradient-top: linear-gradient(0deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-right: linear-gradient(90deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-bottom: linear-gradient(180deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-left: linear-gradient(270deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-top-right: linear-gradient(45deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-bottom-right: linear-gradient(135deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-top-left: linear-gradient(225deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-bottom-left: linear-gradient(315deg, #cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);
$gradient-radial: radial-gradient(#cdddddff, #db3069ff, #ffa400ff, #7b7263ff, #04080fff);

SOME COOL CODEPENS OF CSS ANIMATIONS
BEACH VIBES GRADIENT w ANIMATION: https://codepen.io/chrishodges27/pen/QxGdmb
YELLOW WORD SERIES: https://codepen.io/alvarotrigo/pen/PoKMyWE
-->

#### Notes

Thinking about doing a maximalist/minimalist thing, a play on light/dark mode.

- More fonts and colors for maximalist page, maybe even have a background. Not sure on that though, maybe make one on canva?

- Minimalist page will be straight up black and white with clean animations.

- Spotify should definitely be a thing, to showcase being able to use an API embed.

- Animated background for maximalist version.
