import Link from 'next/link';
import Image from 'next/image';
import Head from 'next/head';
import { useState } from 'react';
import styles from './styles.module.css'

export default function FirstPost() {
  const [mode, setMode] = useState('light');

  const toggleMode = () => {
    setMode(prevMode => (prevMode === 'light' ? 'dark' : 'light'));
  };

  return (
    <div className={`${styles.container} ${mode === 'dark' ? styles['dark-mode'] : styles['light-mode']}`}>
        <Head>
            <title>Main Page</title>
            <link rel="icon" href="/favicon.ico" />
            <link rel="preconnect" href="https://fonts.googleapis.com" crossOrigin="true" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
            <link href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@200;300;400&family=Ms+Madi&family=Oxygen:wght@300;400&display=swap" rel="stylesheet" />
      </Head>
      <div className="sidebar-nav">
        <h2>
          <Link href="/">Back to home</Link>
        </h2>
        <label>
          <input type="checkbox" checked={mode === 'dark'} onChange={toggleMode} />
        </label>
      </div>
      <div className='name-banner'>
      <h1 className='name-banner-text'>David, n0t Dave</h1>
      </div>
      <h1>
  <span>Always</span>
  <div className="message">
    <div className="word1">creative</div>
    <div className="word2">driven</div>
    <div className="word3">honest</div>
  </div>
</h1>
      <div className='link-boxes-div'>
        <a className='linkedin' target="_blank" href="https://www.linkedin.com/in/davidn0tdave">
            <Image className='linkedin-logo' src='/linkedin.png' width={100} height={100} />
        </a>
        <a className='github' target="_blank" href="https://github.com/dfrancisco26">
        <Image className='github-logo' src='/github.png' width={100} height={100} />
        </a>
      </div>
      <div className='business-card-div'>
        <Image className='business-card-img' src='/Business_Card.png' width={450} height={220} />
      </div>
    </div>

  );
}

